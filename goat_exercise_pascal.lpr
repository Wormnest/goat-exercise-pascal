{* GIMP - The GNU Image Manipulation Program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * goat_exercise_pascal.pas
 * GIMP plug-in to show the basics of making a plug-in using pascal.
 * Copyright (C) 2020-2023 Jacob Boerema <jgboerema@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *}

//{$STATIC ON} - not needed anymore see https://wiki.freepascal.org/User_Changes_2.6.0#The_Static_modifier_is_now_always_enabled

program goat_exercise_pascal;

{$mode objfpc}
{$modeswitch autoderef} // No need to pointer dereference Object variables.

uses
  SysUtils, Math, //ctypes,
  // Base Glib and GTK3 libraries
  GLib2, GObject2, Gio2,Gtk3, Gdk3,
  // Gimp related libraries
  Babl0_1, Gegl0_4, Gimp3, GimpUi3,
  // Base unit where we try to hide some of the boiler plate
  gimp_plugin_base;


const
  PLUG_IN_BINARY        = 'goat_exercise_pascal';
  //PLUG_IN_SOURCE      = PLUG_IN_BINARY + '.pas';
  PLUG_IN_PROC          = 'plug-in-goat-exercise-pascal';
  PLUG_IN_ROLE          = 'goat-exercise-pascal';
  PLUG_IN_LANGUAGE      = 'FreePascal';
  GOAT_PLUG_IN_DOMAIN   = 'PascalGoat';

  //  GOAT_URI       = 'https://gitlab.gnome.org/GNOME/gimp/blob/master/plug-ins/goat-exercises/goat_exercise_pascal.pas';
  // Temporary until we have a fixed online URL
  GOAT_URI       = 'https://gitlab.gnome.org/GNOME/gimp/blob/master/extensions/goat-exercises/goat-exercise-c.c';


type
  TGoatHandler = object (TGimpPluginHandler)
  public
    function do_query_procedures(): PGList; virtual;
    function do_create_procedure(const name: PChar): PGimpProcedure; virtual;

    {class function run(proc: PGimpProcedure; run_mode: TGimpRunMode;
                       image: PGimpImage; n_drawables: gint; drawables: PPGimpDrawable;
                       arg: PGimpValueArray; run_data: gpointer): PGimpValueArray; static; cdecl;}
  end;

  TGoat = object (TBasePlugin)
    class function run(proc: PGimpProcedure; run_mode: TGimpRunMode;
                       image: PGimpImage; n_drawables: gint; drawables: PPGimpDrawable;
                       arg: PGimpValueArray; run_data: gpointer): PGimpValueArray; static; cdecl;
  end;


  function TGoatHandler.do_query_procedures(): PGList;
  begin
    g_log(PLUG_IN_DOMAIN, G_LOG_LEVEL_DEBUG, 'Entering TGoatHandler.do_query_procedures', []);
    Result := g_list_append (nil, g_strdup (PLUG_IN_PROC));
    g_log(PLUG_IN_DOMAIN, G_LOG_LEVEL_DEBUG, 'Exiting TGoatHandler.do_query_procedures', []);
  end;

  function TGoatHandler.do_create_procedure(const name: PChar): PGimpProcedure;
  var proc: PGimpProcedure;
  begin
    g_log (PLUG_IN_DOMAIN, G_LOG_LEVEL_DEBUG, 'Entering TGoatHandler.create_procedure', []);
    proc := nil;

    if CompareStr (name, PLUG_IN_PROC) = 0 then begin
      proc := gimp_image_procedure_new ({@Self}ActualPlugin, name,
                                        GIMP_PDB_PROC_TYPE_PLUGIN,
                                        TGimpRunImageFunc(@TGoat.run), nil, nil);

      gimp_procedure_set_image_types (proc, '*');

      gimp_procedure_set_menu_label (proc, 'Exercise a Pascal Goat');
      gimp_procedure_set_icon_name (proc, GIMP_ICON_PLUGIN);  // This pulls in GimpUi3
      gimp_procedure_add_menu_path (proc,
                                    '<Image>/Filters/Development/Goat exercises/');

      gimp_procedure_set_documentation (proc,
                                        'Exercise a goat in the Pascal language',
                                        'Takes a goat for a walk',
                                        PLUG_IN_PROC);
      gimp_procedure_set_attribution (proc,
                                      'Jacob Boerema',
                                      'Jacob Boerema',
                                      '2020-2023');
    end;
    Result := proc;
    g_log (PLUG_IN_DOMAIN, G_LOG_LEVEL_DEBUG, 'Exiting TGoatHandler.create_procedure', []);
  end;



//class function TGoatHandler.run (proc: PGimpProcedure; run_mode: TGimpRunMode;
class function TGoat.run (proc: PGimpProcedure; run_mode: TGimpRunMode;
                          image: PGimpImage; n_drawables: gint; drawables: PPGimpDrawable;
                          arg: PGimpValueArray;
                          run_data: gpointer): PGimpValueArray; static; cdecl;
const
  BUFFER_SIZE = 4096;
var
  status: TGimpPDBStatusType;
  x, y, width, height: gint;
  /////////////
  // Interactive mode variables
  dialog: PGimpDialog;
  help_func: TGimpHelpFunc;
  geometry: TGdkGeometry;
  box, dialog_content_area: PGtkBox;
  head_text, path: PChar;
  widget: PGtkWidget;
  view: PGtkTextView;
  scrolled: PGtkScrolledWindow;
  {$IFDEF READFILE_C_VERSION}
  AFile: PGFile;
  input: PGFileInputStream;
  source_text: array [0..BUFFER_SIZE] of gchar;
  source_text_buf: Pgchar;
  bufsize: gsize;
  nRead: gssize;
  {$ENDIF}
  buffer: PGtkTextBuffer;
  response: gint;
  /////////////
  gbuffer, shadow_buffer: PGeglBuffer;
  MyError: PGError;
  {$IFNDEF READFILE_C_VERSION}
  GotContents: Boolean;
  //contents: Pguint8;
  contents: PChar;
  {$ENDIF}
  drawable: PGimpDrawable;
begin
  g_log(PLUG_IN_DOMAIN, G_LOG_LEVEL_DEBUG, 'Entering run', []);
  status  := GIMP_PDB_SUCCESS;
  MyError := nil;

  if (n_drawables <> 1) then
  begin
    g_set_error (@MyError, error_quark(){GIMP_PLUG_IN_ERROR}, 0,
                 'Procedure ''%s'' only works with one drawable.',
                 [PLUG_IN_PROC]);

    Result := proc.new_return_values (GIMP_PDB_CALLING_ERROR, MyError);
    Exit;
  end
  else
    drawable := drawables[0];

  { In interactive mode, display a dialog to advertise the exercise. }
  if (run_mode = GIMP_RUN_INTERACTIVE) then begin
    g_log(PLUG_IN_DOMAIN, G_LOG_LEVEL_DEBUG, 'Interactive', []);
    gimp_ui_init (PLUG_IN_BINARY);
    help_func := @gimp_standard_help_func;

    dialog := gimp_dialog_new ('Exercise a goat in Pascal', PLUG_IN_ROLE,
                               nil, GTK_DIALOG_USE_HEADER_BAR,
                               TGimpHelpFunc(@help_func),
                               PLUG_IN_PROC,
                               ['_Cancel', GTK_RESPONSE_CANCEL,
                                '_Source', GTK_RESPONSE_APPLY,
                                '_Run',    GTK_RESPONSE_OK,
                                nil]);

    geometry := Default(TGdkGeometry);
    geometry.min_aspect := 0.5;
    geometry.max_aspect := 1.0;
    dialog.set_geometry_hints (nil, @geometry, GDK_HINT_ASPECT);

    box := TGtkBox.new (GTK_ORIENTATION_VERTICAL, 12);
    box.set_border_width (12);
    dialog_content_area := dialog.get_content_area();
    dialog_content_area.add(box);
    box.show();

    {
      Seems that \n in strings is already interpreted by the compiler?
      At least it does not get interpreted here by g_strdup_printf.
      I guess that things like that can be determined at compile time,
      a good reason why it's not interpreted at runtime.
    }
    head_text := g_strdup_printf ( 'This plug-in is an exercise in ''%s'' ' +
                                   'to demo plug-in creation.' + #10 +
                                   'Check out the last version ' +
                                   'of the source code online by ' +
                                   'clicking the "Source" button.',
                                   [PLUG_IN_LANGUAGE]);
    widget := TGtkLabel.new (head_text);
    g_free (head_text);
    box.pack_start (widget, False, False, 1);
    widget.show();

    scrolled := TGtkScrolledWindow.new (nil, nil);
    box.pack_start (scrolled, True, True, 1);
    scrolled.set_vexpand (True);
    scrolled.show ();

    // If we had this plug-in in the default GIMP directory we would use this:
    {$IFDEF GIMP_PATH}
    path := g_build_filename ( gimp_plug_in_directory (),
                              ['plug-ins', PLUG_IN_BINARY, PLUG_IN_SOURCE, nil]);
    {$ELSE}
    // However for now we are using this custom directory and filename:
    path := 'e:\lazdev2\extras\gimp\goat-exercise-pascal\goat_exercise_pascal.lpr';
    {$ENDIF}
    g_log(PLUG_IN_DOMAIN, G_LOG_LEVEL_DEBUG, 'File path: %s', [path]);

    view := TGtkTextView.new ();
    view.set_wrap_mode (GTK_WRAP_WORD);
    view.set_editable (False);
    buffer := view.get_buffer ();

{$IFDEF READFILE_C_VERSION}
    Afile := g_file_new_for_path (path);
    input := g_file_read (AFile, nil, @MyError);
    if (MyError <> nil) then begin
      g_log(PLUG_IN_DOMAIN, G_LOG_LEVEL_ERROR, 'Error: %s', [MyError.message]);
      g_error_free (MyError);
      MyError := nil;
    end;

    bufsize := BUFFER_SIZE;
    source_text_buf := getmem(BUFFER_SIZE);
    nRead := input.read (Pguint8(source_text_buf), BUFFER_SIZE{@bufsize}, nil, @MyError);
    if nRead = -1 then
      g_log(PLUG_IN_DOMAIN, G_LOG_LEVEL_MESSAGE, 'Nothing read!', []);
    while (nRead > 0) do begin
      buffer.insert_at_cursor(Pgchar(source_text_buf), nRead);
      nRead := input.read (Pguint8(source_text_buf), BUFFER_SIZE{@bufsize}, nil, @MyError);
    end;
    if (MyError <> nil) then begin
      g_log(PLUG_IN_DOMAIN, G_LOG_LEVEL_ERROR, 'Error: %s', [MyError.message]);
      g_error_free (MyError);
    end;
    freemem(source_text_buf);

    g_object_unref (PGObject(AFile));
{$ELSE}
    GotContents := g_file_get_contents(path, @contents, nil, @MyError);
    if (GotContents) then begin
      buffer.set_text(contents, -1);
    end
    else if (MyError <> nil) then begin
      g_log(PLUG_IN_DOMAIN, G_LOG_LEVEL_ERROR, 'Error: %s', [MyError.message]);
      g_error_free (MyError);
    end;
{$ENDIF}
{$IFDEF GIMP_PATH}
    g_free (path);
{$ENDIF}

    scrolled.add (view);
    view.show ();

    while True do begin
      response := dialog.run();
      if response = GTK_RESPONSE_OK then begin
        // TODO ERROR IN PARSER? should have been destroy instead of destroy_
        dialog.destroy_();
        break;
      end
      else if (response = GTK_RESPONSE_APPLY) then begin
        (* Show the code. *)
        g_app_info_launch_default_for_uri (GOAT_URI, nil, nil);
        continue;
      end
      else begin (* CANCEL, CLOSE, DELETE_EVENT *)
        dialog.destroy_ ();
        Result := proc.new_return_values ( GIMP_PDB_CANCEL, nil);
        Exit;
      end;
    end;
  end; // if run interactive

  g_log(PLUG_IN_DOMAIN, G_LOG_LEVEL_DEBUG, 'If mask_intersect', []);
  drawable := drawables[0];
  if (drawable.mask_intersect (@x, @y, @width, @height)) then begin
        g_log(PLUG_IN_DOMAIN, G_LOG_LEVEL_DEBUG, 'gegl init', []);
        gegl_init (nil, nil);

        g_log(PLUG_IN_DOMAIN, G_LOG_LEVEL_DEBUG, 'get buffer', []);
        gbuffer       := drawable.get_buffer ();
        shadow_buffer := drawable.get_shadow_buffer ();

        gegl_render_op (gbuffer, shadow_buffer, 'gegl:invert', [nil]);

        g_object_unref (PGObject(shadow_buffer)); (* flushes the shadow tiles *)
        g_object_unref (PGObject(gbuffer));

        drawable.merge_shadow (True);
        drawable.update (x, y, width, height);
        gimp_displays_flush ();

        gegl_exit ();
  end;

  Result := proc.new_return_values (status, nil);
  g_log(PLUG_IN_DOMAIN, G_LOG_LEVEL_DEBUG, 'Exiting run', []);
end;

var
  GoatPluginHandler: TGoatHandler;

begin
  //SetExceptionMask([exInvalidOp, exDenormalized, exZeroDivide, exOverflow, exUnderflow, exPrecision]);
  PLUG_IN_DOMAIN := GOAT_PLUG_IN_DOMAIN;
  GoatPluginHandler.Init;
  ActualPluginHandler := GoatPluginHandler;
  //ActualPluginHandler.Init;
  ExitCode := gimp_main(TGoat.get_type(), argc, Pointer(argv));
end.

