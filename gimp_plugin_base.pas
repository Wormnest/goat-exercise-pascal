{* GIMP - The GNU Image Manipulation Program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * gimp_plugin_base.pas
 * GIMP plug-in base for making a GIMP plug-in using pascal easier.
 * Copyright (C) 2020-2023 Jacob Boerema <jgboerema@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *}

unit gimp_plugin_base;

{$mode objfpc}{$H+}
{$modeswitch autoderef} // No need to pointer dereference Object variables.

interface

uses
  SysUtils,
  // Base Glib and GTK3 libraries
  GLib2, GObject2, //Gio2,Gtk3, Gdk3,
  // Gimp related libraries
  {Babl0_1, Gegl0_4,} Gimp3, GimpUi3;

var
  PLUG_IN_DOMAIN: PChar = 'freepascal_gimp_plugin';
  PLUG_IN_PROC  : PChar = 'base-gimp-plug-in-freepascal';

type
  TGimpPluginHandler = object
  private
  protected
    ActualPlugin: PGimpPlugin;
  public
    constructor Init;
    destructor Done;

    function do_query_procedures(): PGList; virtual;
    function do_create_procedure(const name: PChar): PGimpProcedure; virtual;
  end;
  PGimpPluginHandler = ^TGimpPluginHandler;

  PBasePluginClass = ^TBasePluginClass;
  TBasePluginClass = object (TGimpPluginClass)
    class procedure init(klass: PBasePluginClass; {= @Self} class_data: gpointer); static; cdecl;
  end;


  PBasePlugin = ^TBasePlugin;
  TBasePlugin = object (TGimpPlugin)
  private

  protected
    parent_instance: PGimpPlugin;

    // TODO: Test if static is needed here!
    procedure init(APlugin: PBasePlugin); static; cdecl;
    function get_type: TGType; static; cdecl;

    // TODO: Probably make these two functions als class functions.
    function query_procedures(plug_in: PGimpPlugin {= Self}): PGList; {static;} cdecl;
    function create_procedure({plug_in: PGimpPlugin;} {= Self} const name: PChar):
                              PGimpProcedure; {static;} cdecl;

  public

(*
    class function run(proc: PGimpProcedure; run_mode: TGimpRunMode;
                       image: PGimpImage; n_drawables: gint; drawables: PPGimpDrawable;
                       arg: PGimpValueArray; run_data: gpointer): PGimpValueArray; static; cdecl;
*)
  end;

var
  ActualPluginHandler: TGimpPluginHandler;

implementation

uses Math;

const
  gimp_plugin_parent_class:   PGTypeClass = nil;
  gimp_plugin_private_offset: gint        = 0;

  GIMP_PLUGIN_TYPE: TGType = 0;

constructor TGimpPluginHandler.Init;
begin
end;

destructor TGimpPluginHandler.Done;
begin
end;

function TGimpPluginHandler.do_query_procedures(): PGList;
begin
  g_log(PLUG_IN_DOMAIN, G_LOG_LEVEL_DEBUG, 'Entering do_query_procedures', []);
  //Result := g_list_append (nil, g_strdup (PLUG_IN_PROC));
  Result := nil;
  g_log(PLUG_IN_DOMAIN, G_LOG_LEVEL_DEBUG, 'Exiting do_query_procedures', []);
end;

function TGimpPluginHandler.do_create_procedure(const name: PChar): PGimpProcedure;
begin
  Result := nil;
end;


{ Order of calls to a GIMP plugin:

  On initialization:
  1. TBasePlugin.get_type
  2. TBasePluginClass.init
  3. TBasePlugin.init
  4. query_procedures
  5. create_procedure

  On run:
  1. TBasePlugin.get_type
  2. TBasePluginClass.init
  3. TBasePlugin.init
  4. create_procedure
  5. run procedure
}

class procedure TBasePluginClass.init(klass: PBasePluginClass; {= @Self} class_data: gpointer); static; cdecl;
var
  plug_in_class: PGimpPluginClass;
begin
  g_log(PLUG_IN_DOMAIN, G_LOG_LEVEL_DEBUG, 'TBasePluginClass.init: class data = %p, Self = %p', [class_data, @klass]);
  if klass = nil then begin
    g_log(PLUG_IN_DOMAIN, G_LOG_LEVEL_ERROR, 'Something is wrong: parameter klass = nil!', []);
    Exit;
  end;

  gimp_plugin_parent_class := g_type_class_peek_parent(PGTypeClass(klass));
  if (gimp_plugin_private_offset <> 0) then
    g_type_class_adjust_private_offset (klass, @gimp_plugin_private_offset);

  plug_in_class := PGimpPluginClass(g_type_check_class_cast(PGTypeClass(klass), gimp_plug_in_get_type ()));

  if plug_in_class = nil then begin
    g_log(PLUG_IN_DOMAIN, G_LOG_LEVEL_ERROR, 'Something is wrong: plug_in_class = nil!', []);
    Exit;
  end;

  Pointer(plug_in_class^.query_procedures) := @TBasePlugin.query_procedures;
  Pointer(plug_in_class^.create_procedure) := @TBasePlugin.create_procedure;

  g_log(PLUG_IN_DOMAIN, G_LOG_LEVEL_DEBUG, 'EXIT TBasePluginClass.init', []);
end;


procedure TBasePlugin.init(APlugin: PBasePlugin); static; cdecl;
begin
  g_log(PLUG_IN_DOMAIN, G_LOG_LEVEL_DEBUG, 'TBasePlugin.init - nothing to do', []);
end;

function base_plugin_get_instance_private(_self: PBasePlugin): gpointer;
begin
  g_log(PLUG_IN_DOMAIN, G_LOG_LEVEL_DEBUG, 'base_plugin_get_instance_private', []);

  Result := _self + gimp_plugin_private_offset;
end;

function TBasePlugin.get_type(): TGType; static; cdecl;
var
  Info: TGTypeInfo;
  type_id: TGType;
begin
  g_log(PLUG_IN_DOMAIN, G_LOG_LEVEL_DEBUG, 'Entering get_type', []);

  if g_once_init_enter(@GIMP_PLUGIN_TYPE) then begin
    Info.class_size     := SizeOf(TBasePluginClass);
    Info.base_init      := nil;
    Info.base_finalize  := nil;
    Info.class_init     := TGClassInitFunc(@TBasePluginClass.init);
    Info.class_finalize := nil;
    Info.class_data     := nil;
    Info.instance_size  := SizeOf(TBasePlugin);
    Info.n_preallocs    := 0;
    Info.instance_init  := TGInstanceInitFunc(@TBasePlugin.init);
    Info.value_table    := nil;

    type_id := g_type_register_static(gimp_plug_in_get_type(), g_intern_static_string(PLUG_IN_DOMAIN), @Info, 0);
    g_once_init_leave(@GIMP_PLUGIN_TYPE, type_id);
  end;
  Result := GIMP_PLUGIN_TYPE;

  g_log(PLUG_IN_DOMAIN, G_LOG_LEVEL_DEBUG, 'Exiting get_type', []);
end;

function TBasePlugin.query_procedures( plug_in: PGimpPlugin): PGList; {static;} cdecl;
begin
  g_log(PLUG_IN_DOMAIN, G_LOG_LEVEL_DEBUG, 'Entering query_procedures', []);

  Result := ActualPluginHandler.do_query_procedures();

  g_log(PLUG_IN_DOMAIN, G_LOG_LEVEL_DEBUG, 'Exiting query_procedures', []);
end;

function TBasePlugin.create_procedure( {plug_in: PGimpPlugin;} const name: PChar): PGimpProcedure; {static;} cdecl;
begin
  g_log (PLUG_IN_DOMAIN, G_LOG_LEVEL_DEBUG, 'Entering create_procedure', []);
  ActualPluginHandler.ActualPlugin := @Self;
  Result := ActualPluginHandler.do_create_procedure(name);
  g_log (PLUG_IN_DOMAIN, G_LOG_LEVEL_DEBUG, 'Exiting create_procedure', []);
end;


initialization
  SetExceptionMask([exInvalidOp, exDenormalized, exZeroDivide, exOverflow, exUnderflow, exPrecision]);
end.

